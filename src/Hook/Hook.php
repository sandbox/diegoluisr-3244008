<?php

namespace Drupal\hkhelper\Hook;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Class name.
 */
abstract class Hook implements ContainerInjectionInterface {

  /**
   * Function detail.
   */
  abstract public function call(array $aparam = []);

}
