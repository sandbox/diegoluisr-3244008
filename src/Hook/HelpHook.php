<?php

namespace Drupal\hkhelper\Hook;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class detail.
 */
class HelpHook extends Hook {

  use StringTranslationTrait;

  /**
   * Var that store the service.
   *
   * @var Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(ModuleHandlerInterface $moduleHandler) {
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this class.
    return new static(
      $container->get('module_handler')
    );
  }

  /**
   * Function detail.
   */
  public function call(array $params = []) {
    // Params: $route_name, RouteMatchInterface $route_match.
    extract($params, EXTR_OVERWRITE | EXTR_REFS);

    if ($route_name == 'help.page.hkhelper') {
      $output = '';
      $output .= '<h3>' . $this->t('About (@moduleName)', [
        '@moduleName' => $this->moduleHandler->getModule('hkhelper')->getName(),
      ]) . '</h3>';
      $output .= '<p>' . $this->t('The Hook Helper module allows to handle module hooks in a different way') . '</p>';
      $output .= '<h3>' . $this->t('Uses') . '</h3>';
      $output .= '<p>' . $this->t('Please, check the code to see this call is self described') . '</p>';
      return $output;
    }
  }

}
