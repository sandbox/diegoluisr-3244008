<?php

namespace Drupal\hkhelper;

use Drupal\hkhelper\Hook\Hook;

/**
 * The hook factory.
 */
class HookFactory {

  /**
   * Contructor function comment.
   */
  public function __construct() {
  }

  /**
   * Build function comment.
   */
  public static function build(string $moduleName = 'hkhelper', string $hookName = NULL) {
    $className = self::getClassName(str_replace($moduleName . '_', '', $hookName));
    $class = 'Drupal\\' . $moduleName . '\\Hook\\' . $className . 'Hook';

    if (class_exists($class)) {
      $instance = $class::create(\Drupal::getContainer());
      if ($instance instanceof Hook) {
        return $instance;
      }
    }

    \Drupal::logger('hkhelper')->notice('Class not found: ' . $class);

    return NULL;
  }

  /**
   * Function to conver hook name to class name candidate.
   */
  private static function getClassName(string $hookName) {
    return str_replace('_', '', ucwords($hookName, '_'));
  }

}
